var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
const dbUrl = 'mongodb://localhost:27017/';
const dbName = 'db_dsi';
const collectionName = 'tasks';

router.get('/', function(req, res, next) {
  const createdAt = req.query.date ? req.query.date : new Date().toISOString();
  connectToDb((dbo) => {
    return new Promise((resolve, reject) => {
      dbo
        .collection(collectionName)
        .find({ createdAt })
        .toArray(function(error, result) {
          if (error) {
            console.error(error);
            reject();
            throw error;
          }
          res.json(transformTasks(result));
          resolve();
        });
    });
  });
});

router.post('/', function(req, res, next) {
  connectToDb((dbo) => {
    return new Promise((resolve, reject) => {
      const task = req.body;
      dbo.collection(collectionName).insertOne(task, function(error, resource) {
        if (error) {
          console.error(error);
          reject();
          throw error;
        }
        res.json(transformTask(task));
        resolve();
      });
    });
  });
});

router.put('/', (req, res) => {
  connectToDb((dbo) => {
    return new Promise((resolve, reject) => {
      const task = req.body;
      dbo
        .collection(collectionName)
        .updateOne(
          { _id: ObjectId(task.id) },
          { $set: getTaskWithoutId(task) },
          (error, resource) => {
            if (error) {
              reject();
              throw error;
            }
            res.json(task);
            resolve();
          }
        );
    });
  });

  function getTaskWithoutId(task) {
    const taskCopy = { ...task };
    delete taskCopy.id;
    return taskCopy;
  }
});

router.delete('/:id', (req, res, next) => {
  connectToDb((dbo) => {
    return new Promise((resolve, reject) => {
      dbo
        .collection(collectionName)
        .findOne({ _id: ObjectId(req.params.id) }, (findError, findResult) => {
          if (findError) {
            reject();
            throw findError;
          }
          dbo.collection(collectionName).deleteOne({ _id: findResult._id }, (removeError, obj) => {
            if (removeError) {
              reject();
              throw removeError;
            }
            res.json(findResult);
            resolve();
          });
        });
    });
  });
});

module.exports = router;

function connectToDb(callback) {
  MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, db) => {
    if (err) {
      console.error(err);
      throw err;
    }
    const dbo = db.db(dbName);
    callback(dbo).finally(() => db.close());
  });
}

function transformTasks(tasks) {
  let transformedTasks = [];
  tasks.forEach((task) => {
    transformedTasks.push(transformTask(task));
  });
  return transformedTasks;
}

function transformTask(task) {
  return {
    id: task._id,
    name: task.name,
    startedAt: task.startedAt,
    finishedAt: task.finishedAt,
    description: task.description,
    status: task.status,
    createdAt: task.createdAt
  };
}
