import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action, Store, select } from '@ngrx/store';
import { map, withLatestFrom } from 'rxjs/operators';

import * as AppActions from '../actions/app.actions';
import * as TasksActions from '../tasks/actions/tasks.actions';
import * as fromApp from '../reducers';

@Injectable()
export class AppEffects {
  constructor(private actions$: Actions, private store: Store<fromApp.State>) {}

  @Effect()
  setDate$: Observable<Action> = this.actions$.pipe(
    ofType(AppActions.AppActionTypes.SetDate),
    withLatestFrom(this.store.pipe(select(fromApp.getDate))),
    map(([action, date]) => ({
      type: TasksActions.TasksActionTypes.Init,
      payload: date
    }))
  );
}
