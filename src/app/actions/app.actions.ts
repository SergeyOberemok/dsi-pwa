import { Action } from '@ngrx/store';

export enum AppActionTypes {
  SetDate = '[App] Set Date'
}

export class SetDate implements Action {
  readonly type = AppActionTypes.SetDate;

  constructor(public payload: string) {}
}

export type AppActions = SetDate;
