import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromApp from './reducers';
import { Observable } from 'rxjs';
import * as AppActions from './actions/app.actions';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(private store: Store<fromApp.State>) {}

  public get date$(): Observable<string> {
    return this.store.pipe(select(fromApp.getDate));
  }

  public dispatchSetDate(date: string) {
    this.store.dispatch(new AppActions.SetDate(date));
  }
}
