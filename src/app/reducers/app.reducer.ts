import * as AppActions from '../actions/app.actions';

export interface State {
  date: string;
}

export const initialState: State = {
  date: null
};

export function reducer(state = initialState, action: AppActions.AppActions): State {
  switch (action.type) {
    case AppActions.AppActionTypes.SetDate:
      return {
        ...state,
        date: action.payload
      };

    default:
      return state;
  }
}
