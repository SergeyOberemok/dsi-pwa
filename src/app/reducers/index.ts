import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromApp from './app.reducer';

export interface State {
  app: fromApp.State;
}

export const reducers: ActionReducerMap<State> = {
  app: fromApp.reducer
};

const getAppState = createFeatureSelector<fromApp.State>('app');

export const getDate = createSelector(
  getAppState,
  (state: fromApp.State) => state.date
);

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
