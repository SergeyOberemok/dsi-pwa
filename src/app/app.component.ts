import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { TasksService } from './tasks/tasks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'app';

  constructor(private router: Router) {}

  public get environment(): string {
    return environment.production ? 'production' : 'development';
  }

  ngOnInit() {
    this.router.navigate(['/tasks']);
  }

  ngOnDestroy() {}
}
