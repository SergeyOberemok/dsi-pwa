import { Action } from '@ngrx/store';
import { Task } from '../../core/shared';

export const enum TasksActionTypes {
  Default = '[Tasks] Task Default',
  Init = '[Tasks] Init',
  Load = '[Tasks] Load Tasks',
  CreateTask = '[Tasks] Create Task',
  Update = '[Tasks] Update Tasks',
  RemoveTask = '[Tasks] Remove Task',
  Running = '[Tasks] Task is running',
  ChangeLoading = '[Tasks] Change tasks loading'
}

export class Init implements Action {
  readonly type = TasksActionTypes.Init;

  constructor(public payload: string) {}
}

export class Load implements Action {
  readonly type = TasksActionTypes.Load;

  constructor(public payload: Task[]) {}
}

export class CreateTask implements Action {
  readonly type = TasksActionTypes.CreateTask;

  constructor(public payload: Task) {}
}

export class Update implements Action {
  readonly type = TasksActionTypes.Update;

  constructor(public payload: Task) {}
}

export class RemoveTask implements Action {
  readonly type = TasksActionTypes.RemoveTask;

  constructor(public payload: Task) {}
}

export class Running implements Action {
  readonly type = TasksActionTypes.Running;

  constructor(public payload: boolean) {}
}

export class ChangeLoading implements Action {
  readonly type = TasksActionTypes.ChangeLoading;

  constructor(public payload: boolean) {}
}

export type TasksActions = Init | Load | CreateTask | Update | RemoveTask | Running | ChangeLoading;
