import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Task } from 'src/app/core/shared';
import { TasksService } from '../tasks.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit, OnDestroy {
  public tasks$: Observable<Task[]>;
  public isLoading$: Observable<boolean>;
  public isRunning: boolean;
  private destroy$: Subject<void> = new Subject();

  constructor(private tasksService: TasksService) {}

  ngOnInit() {
    this.isLoading$ = this.tasksService.isLoading$;
    this.tasks$ = this.tasksService.tasks$;
    this.tasksService.isRunning$
      .pipe(takeUntil(this.destroy$))
      .subscribe(isRunning => (this.isRunning = isRunning));
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  public startTask(task: Task) {
    task.start();
    this.tasksService.dispatchUpdateTask(task);
  }

  public resumeTask(task: Task) {
    task.resume();
    this.tasksService.dispatchUpdateTask(task);
  }

  public finishTask(task: Task) {
    task.finish();
    this.tasksService.dispatchUpdateTask(task);
  }

  public removeTask(task: Task) {
    task.finish();
    this.tasksService.dispatchRemoveTask(task);
  }
}
