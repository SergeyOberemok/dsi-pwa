import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Subscription, Observable, from } from 'rxjs';
import {
  takeUntil,
  filter,
  tap,
  map,
  switchMap,
  reduce,
  distinctUntilChanged
} from 'rxjs/operators';

import { TaskStatus, Task, IsoDate } from '../core/shared';
import { TasksService } from './tasks.service';
import { AppService } from '../app.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit, OnDestroy {
  public tasks$: Observable<Task[]> = null;
  public isRunning: boolean;
  public duration: number;
  public date: FormControl = null;
  private destroy$: Subject<void> = new Subject();
  private tickSubscription: Subscription;

  constructor(
    private tasksService: TasksService,
    private appService: AppService
  ) {
    this.duration = 0;
  }

  ngOnInit() {
    this.initDate();
    this.tasks$ = this.tasksService.tasks$;
    this.tasksService.isRunning$
      .pipe(
        tap(isRunning =>
          isRunning ? this.date.disable() : this.date.enable()
        ),
        takeUntil(this.destroy$)
      )
      .subscribe(isRunning => (this.isRunning = isRunning));

    this.countDuration(this.tasks$);
    this.bindTick(this.tasks$);
    this.unbindTick(this.tasks$);
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  public createTask($event: Task) {
    this.tasksService.dispatchCreateTask($event);
  }

  public get date$(): Observable<string> {
    return this.appService.date$;
  }

  private initDate() {
    const currentDate = new IsoDate();
    this.date = new FormControl(currentDate.getDate());
    this.date.valueChanges
      .pipe(
        distinctUntilChanged((prev, next) => prev.getTime() === next.getTime()),
        takeUntil(this.destroy$)
      )
      .subscribe(this.setUpDate.bind(this));
    this.appService.dispatchSetDate(currentDate.getIsoDate());
  }

  private setUpDate($event: Date) {
    this.appService.dispatchSetDate(new IsoDate($event).getIsoDate());
  }

  private countDuration(tasks$: Observable<Task[]>) {
    tasks$
      .pipe(
        tap(() => (this.duration = 0)),
        switchMap(tasks => {
          return from(tasks).pipe(
            filter(task => task.status !== TaskStatus.Started),
            reduce((acc, task: Task) => acc + task.duration, 0)
          );
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(sum => (this.duration = sum));
  }

  private bindTick(tasks$: Observable<Task[]>) {
    tasks$
      .pipe(
        switchMap(tasks => from(tasks)),
        filter(task => task.status === TaskStatus.Started),
        takeUntil(this.destroy$)
      )
      .subscribe(this.tickDuration.bind(this));
  }

  private tickDuration(runningTask: Task) {
    this.tasksService.dispatchRunning(true);
    this.tickSubscription = runningTask.tick.subscribe(
      ((duration, context) => {
        return tickDuration => (context.duration = tickDuration + duration);
      })(this.duration, this)
    );
  }

  private unbindTick(tasks$: Observable<Task[]>) {
    tasks$
      .pipe(
        map(tasks => tasks.find(task => task.status === TaskStatus.Started)),
        filter(task => task === undefined),
        takeUntil(this.destroy$)
      )
      .subscribe(this.unsubscribeTick.bind(this));
  }

  private unsubscribeTick() {
    if (this.tickSubscription) {
      this.tickSubscription.unsubscribe();
      this.tasksService.dispatchRunning(false);
    }
  }
}
