import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import * as fromTasks from './tasks.reducer';

export interface State {
  tasks: fromTasks.State;
}

export const reducers: ActionReducerMap<State> = {
  tasks: fromTasks.reducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

const getTasksState = createFeatureSelector<State>('tasks');

export const getIsTasksLoading = createSelector(
  getTasksState,
  (state: State) => state.tasks.isLoading
);
export const getTasks = createSelector(
  getTasksState,
  (state: State) => state.tasks.tasks
);

export const getIsRunning = createSelector(
  getTasksState,
  (state: State) => state.tasks.isRunning
);
