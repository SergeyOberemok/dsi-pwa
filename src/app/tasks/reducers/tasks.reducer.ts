import { Task } from '../../core/shared';
import * as TasksActions from '../actions/tasks.actions';

export interface State {
  isLoading: boolean;
  tasks: Task[];
  isRunning: boolean;
}

const initialState: State = {
  isLoading: false,
  tasks: [],
  isRunning: false
};

export function reducer(state: State = initialState, action: TasksActions.TasksActions): State {
  switch (action.type) {
    case TasksActions.TasksActionTypes.Init:
      return { ...state };
    case TasksActions.TasksActionTypes.Load:
      return {
        ...state,
        isLoading: false,
        tasks: action.payload
      };
    case TasksActions.TasksActionTypes.CreateTask: {
      const tasks = Object.assign([], state.tasks);
      tasks.push(action.payload);
      return {
        ...state,
        tasks
      };
    }
    case TasksActions.TasksActionTypes.Update:
      return {
        ...state,
        tasks: Object.assign([], state.tasks)
      };
    case TasksActions.TasksActionTypes.RemoveTask: {
      const tasks = Object.assign([], state.tasks);
      const index = tasks.indexOf(action.payload);
      tasks.splice(index, 1);
      return {
        ...state,
        tasks
      };
    }
    case TasksActions.TasksActionTypes.Running:
      return {
        ...state,
        isRunning: action.payload
      };
    case TasksActions.TasksActionTypes.ChangeLoading:
      return {
        ...state,
        isLoading: action.payload
      };
    default:
      return state;
  }
}
