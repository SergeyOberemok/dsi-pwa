import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { take } from 'rxjs/operators';
import { AppService } from '../../app.service';
import { Task } from '../../core/shared';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit {
  public createTaskForm: FormGroup;
  @Output() create: EventEmitter<Task> = new EventEmitter();

  constructor(private readonly appService: AppService) {}

  ngOnInit() {
    this.createTaskForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('')
    });
  }

  public onSubmit() {
    this.appService.date$.pipe(take(1)).subscribe(date => {
      this.create.emit(
        new Task({
          name: this.createTaskForm.get('name').value,
          createdAt: date,
          description: this.createTaskForm.get('description').value
        })
      );
    });
    this.createTaskForm.reset();
  }
}
