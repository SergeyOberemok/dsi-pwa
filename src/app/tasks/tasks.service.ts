import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store, select } from '@ngrx/store';
import { Observable, from } from 'rxjs';
import { map, switchMap, toArray, concatMap, take, finalize } from 'rxjs/operators';

import * as fromTasks from './reducers';
import * as TasksActions from './actions/tasks.actions';
import { Task, TaskDto } from '../core/shared';
import { API_URLS } from '../core/shared';

@Injectable()
export class TasksService {
  constructor(private http: HttpClient, private store: Store<fromTasks.State>) {}

  public fetchTasks(date: string): Observable<Task[]> {
    this.store.dispatch(new TasksActions.ChangeLoading(true));

    return this.http.get<Task[]>(API_URLS.tasks.all, { params: { date }}).pipe(
      switchMap((tasks) => from(tasks)),
      map((task) => Object.assign(new Task(), task)),
      toArray(),
      finalize(() => this.store.dispatch(new TasksActions.ChangeLoading(false)))
    );
  }

  public create(task: Task): Observable<Task> {
    return new Observable((observer) => {
      this.http
        .post<TaskDto>(API_URLS.tasks.create, task.exportDto())
        .pipe(
          map((newTask) => {
            task.id = newTask.id;
            return task;
          })
        )
        .subscribe(
          (newTask) => {
            observer.next(newTask);
            observer.complete();
          },
          (error) => {
            console.error(error);
            observer.error(error);
          }
        );
    });
  }

  public update(task: Task): Observable<Task> {
    return new Observable((observer) => {
      this.http.put<TaskDto>(API_URLS.tasks.update, task.exportDto()).subscribe(
        () => {
          observer.next(task);
          observer.complete();
        },
        (error) => {
          console.error(error);
          observer.error(error);
        }
      );
    });
  }

  public remove(task: Task): Observable<Task> {
    return new Observable((observer) => {
      this.http.delete<TaskDto>(API_URLS.tasks.delete + '/' + task.id).subscribe(
        () => {
          observer.next(task);
          observer.complete();
        },
        (error) => {
          console.error(error);
          observer.error(error);
        }
      );
    });
  }

  public get tasks$(): Observable<Task[]> {
    return this.store.pipe(select(fromTasks.getTasks));
  }

  public get isLoading$(): Observable<boolean> {
    return this.store.pipe(select(fromTasks.getIsTasksLoading));
  }

  public get isRunning$(): Observable<boolean> {
    return this.store.pipe(select(fromTasks.getIsRunning));
  }

  public dispatchInitTasks(date: string) {
    this.store.dispatch(new TasksActions.Init(date));
  }

  public dispatchCreateTask(task: Task) {
    this.store.dispatch(new TasksActions.CreateTask(task));
  }

  public dispatchUpdateTask(task: Task) {
    this.store.dispatch(new TasksActions.Update(task));
  }

  public dispatchRemoveTask(task: Task) {
    this.store.dispatch(new TasksActions.RemoveTask(task));
  }

  public dispatchRunning(isRunning: boolean) {
    this.store.dispatch(new TasksActions.Running(isRunning));
  }
}
