import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import { Action } from '@ngrx/store';

import { TasksService } from '../tasks.service';
import * as TasksActions from '../actions/tasks.actions';
import { Task } from 'src/app/core/shared';

@Injectable()
export class TasksEffects {
  constructor(private actions$: Actions, private tasksService: TasksService) {}

  @Effect()
  init$: Observable<Action> = this.actions$.pipe(
    ofType(TasksActions.TasksActionTypes.Init),
    mergeMap((action: TasksActions.Init) =>
      this.tasksService.fetchTasks(action.payload).pipe(
        map((tasks: Task[]) => ({
          type: TasksActions.TasksActionTypes.Load,
          payload: tasks
        }))
      )
    )
  );

  @Effect()
  create$: Observable<Action> = this.actions$.pipe(
    ofType(TasksActions.TasksActionTypes.CreateTask),
    mergeMap((action: TasksActions.CreateTask) =>
      this.tasksService
        .create(action.payload)
        .pipe(map(() => ({ type: TasksActions.TasksActionTypes.Default })))
    )
  );

  @Effect()
  update$: Observable<Action> = this.actions$.pipe(
    ofType(TasksActions.TasksActionTypes.Update),
    mergeMap((action: TasksActions.Update) =>
      this.tasksService
        .update(action.payload)
        .pipe(map(() => ({ type: TasksActions.TasksActionTypes.Default })))
    )
  );

  @Effect()
  remove$: Observable<Action> = this.actions$.pipe(
    ofType(TasksActions.TasksActionTypes.RemoveTask),
    mergeMap((action: TasksActions.RemoveTask) =>
      this.tasksService
        .remove(action.payload)
        .pipe(map(() => ({ type: TasksActions.TasksActionTypes.Default })))
    )
  );
}
