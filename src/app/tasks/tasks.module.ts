import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromTasks from './reducers';
import { EffectsModule } from '@ngrx/effects';
import { TasksEffects } from './effects/tasks.effects';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { ClipboardModule } from 'ngx-clipboard';
import { MatTooltipModule } from '@angular/material/tooltip';

import { TasksComponent } from './tasks.component';
import { NewTaskComponent } from './new-task/new-task.component';
import { TaskComponent } from './task/task.component';
import { TimeReadablePipe } from '../core/shared';
import { TaskListComponent } from './task-list/task-list.component';
import { TasksRoutingModule } from './tasks-routing.module';
import { TasksService } from './tasks.service';

@NgModule({
  declarations: [
    TasksComponent,
    NewTaskComponent,
    TaskComponent,
    TimeReadablePipe,
    TaskListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TasksRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    // TODO: Move to mat material shared module
    MatTooltipModule,
    ReactiveFormsModule,
    ClipboardModule,
    StoreModule.forFeature('tasks', fromTasks.reducers, {
      metaReducers: fromTasks.metaReducers
    }),
    EffectsModule.forFeature([TasksEffects])
  ],
  providers: [TasksService]
})
export class TasksModule {}
