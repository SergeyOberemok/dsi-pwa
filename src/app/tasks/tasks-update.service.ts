import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Injectable({
  providedIn: 'root'
})
export class TasksUpdateService {

  constructor(private updates: SwUpdate) {
    updates.available.subscribe(event => {
      console.log('available');
      console.log(event);
    });
    updates.activated.subscribe(event => {
      console.log('activated');
      console.log(event);
    });
  }
}
