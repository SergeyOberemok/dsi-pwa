import { Injectable, ApplicationRef } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { first } from 'rxjs/operators';
import { interval, concat } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TasksCheckForUpdateService {
  constructor(appRef: ApplicationRef, updates: SwUpdate) {
    const isAppStable$ = appRef.isStable.pipe(first((stable) => stable === true));
    const updateInterval = interval(3 * 3000);
    concat(isAppStable$, updateInterval).subscribe(() => updates.checkForUpdate());
  }
}
