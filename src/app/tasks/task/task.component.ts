import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task, TaskStatus } from '../../core/shared';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  @Input() item: Task;
  @Output() started: EventEmitter<Task> = new EventEmitter();
  @Output() resumed: EventEmitter<Task> = new EventEmitter();
  @Output() finished: EventEmitter<Task> = new EventEmitter();
  @Output() removed: EventEmitter<Task> = new EventEmitter();
  @Input() isOnHold: boolean;

  constructor() {}

  ngOnInit() {}

  public startClicked($event: MouseEvent) {
    this.started.emit(this.item);
  }

  public resumeClicked($event: MouseEvent) {
    this.resumed.emit(this.item);
  }

  public finishClicked($event: MouseEvent) {
    this.finished.emit(this.item);
  }

  public removeClicked($event: MouseEvent) {
    this.removed.emit(this.item);
  }

  public get Status() {
    return TaskStatus;
  }
}
