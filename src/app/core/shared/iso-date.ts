export class IsoDate {
  constructor(private date: Date = null) {
    try {
      date.getDate();
    } catch (e) {
      this.date = new Date();
    }
  }

  public getIsoDate(): string {
    let day: string = this.date.getDate().toString();
    day = (+day > 10 ? '' : '0') + day;
    let month = (this.date.getMonth() + 1).toString();
    month = (+month > 10 ? '' : '0') + month;
    return `${this.date.getFullYear()}-${month}-${day}`;
  }

  public getDate(): Date {
    return new Date(this.date.getTime());
  }
}
