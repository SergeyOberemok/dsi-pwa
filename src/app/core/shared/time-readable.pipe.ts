import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeReadable'
})
export class TimeReadablePipe implements PipeTransform {
  transform(value: number): string {
    if (value === 0) {
      return value.toString();
    }
    try {
      const hours = Math.floor(value / 3600);
      const minutes = Math.floor((value - hours * 3600) / 60);
      const seconds = (value - hours * 3600) - minutes * 60;
      return `${hours}h ${minutes}m ${seconds}s`;
    } catch (error) {
      return value.toString();
    }
  }
}
