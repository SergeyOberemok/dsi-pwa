import { BehaviorSubject, Observable } from 'rxjs';
import { IsoDate } from './iso-date';

export enum TaskStatus {
  New,
  Started,
  Finished
}

export interface TaskDto {
  id: string;
  name: string;
  startedAt: number;
  finishedAt: number;
  description: string;
  status: TaskStatus;
  createdAt: string;
}

export class Task implements TaskDto {
  id: string;
  name: string;
  startedAt: number;
  finishedAt: number;
  description: string;
  status: TaskStatus;
  createdAt: string;
  private interval: any;
  private _tick: BehaviorSubject<number> = null;

  constructor(params: Partial<TaskDto> = {}) {
    this.name = params.name ? params.name : '';
    this.description = params.description || '';
    this.startedAt = 0;
    this.finishedAt = 0;
    this.status = TaskStatus.New;
    this.createdAt = params.createdAt
      ? params.createdAt
      : new IsoDate().getIsoDate();
    this._tick = new BehaviorSubject(this.duration);
  }

  public start() {
    this.status = TaskStatus.Started;
    const date = new Date();
    const year = this.createdAt.slice(0, 4);
    date.setFullYear(+year);
    const month = +this.createdAt.slice(5, 7) - 1;
    date.setMonth(+month);
    const day = this.createdAt.slice(8);
    date.setDate(+day);
    this.startedAt = this.finishedAt = date.getTime();
    this.run();
  }

  public finish() {
    this.status = TaskStatus.Finished;
    this.stop();
  }

  public resume() {
    this.status = TaskStatus.Started;
    this.run();
  }

  public get isFinished(): boolean {
    return this.status === TaskStatus.Finished;
  }

  private run() {
    this.interval = setInterval(() => {
      this.finishedAt = new Date().getTime();
      this._tick.next(this.duration);
    }, 1000);
  }

  private stop() {
    clearInterval(this.interval);
  }

  public get tick(): Observable<number> {
    return this._tick.asObservable();
  }

  public exportDto(): TaskDto {
    return {
      id: this.id,
      name: this.name,
      startedAt: this.startedAt,
      finishedAt: this.finishedAt,
      description: this.description,
      status: this.status,
      createdAt: this.createdAt
    };
  }

  public get duration(): number {
    return Math.floor((this.finishedAt - this.startedAt) / 1000);
  }
}
