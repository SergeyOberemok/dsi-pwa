import { environment } from '../../../environments/environment';

export const API_URLS = {
  tasks: {
    all: `${environment.apiHost}/tasks`,
    create: `${environment.apiHost}/tasks`,
    update: `${environment.apiHost}/tasks`,
    delete: `${environment.apiHost}/tasks`,
  }
};
